function urlB64ToUint8Array(base64String) {
	  const padding = '='.repeat((4 - base64String.length % 4) % 4);
	  const base64 = (base64String + padding)
	    .replace(/\-/g, '+')
	    .replace(/_/g, '/');

	  const rawData = window.atob(base64);
	  const outputArray = new Uint8Array(rawData.length);

	  for (let i = 0; i < rawData.length; ++i) {
	    outputArray[i] = rawData.charCodeAt(i);
	  }
	  return outputArray;
	}

 window.vapidPublicKey = urlB64ToUint8Array('BAoZugrcUWwi7MDW255YRxjmKJ6GPlvFauuU8bGT_150UJyAEvZNCWloHvrcMbl1IRqmQvHvynJQND_DF7JATyM');
 
 
//Let's check if the browser supports notifications
 if (!("Notification" in window)) {
   console.error("This browser does not support desktop notification");
 }

 // Let's check whether notification permissions have already been granted
 else if (Notification.permission === "granted") {
   console.log("Permission to receive notifications has been granted");
 }

 // Otherwise, we need to ask the user for permission
 else if (Notification.permission !== 'denied') {
   Notification.requestPermission(function (permission) {
   // If the user accepts, let's create a notification
     if (permission === "granted") {
       console.log("Permission to receive notifications has been granted");
     }
   });
 }
 function updateSubscriptionOnServer(subscription) {
	  // TODO: Send subscription to application server
	  if (subscription) {
		 console.log('user subscription details');
	    console.log(JSON.stringify(subscription));
	  } else {
		  console.log('no subscription found');
	  }
	}
 function initialiseUI() {
	
	   
	      subscribeUser();
	   
	  

	  // Set the initial subscription value
	  swRegistration.pushManager.getSubscription()
	  .then(function(subscription) {
	    isSubscribed = !(subscription === null);

	    updateSubscriptionOnServer(subscription);

	    if (isSubscribed) {
	      console.log('User IS subscribed.');
	    } else {
	      console.log('User is NOT subscribed.');
	    }

	   // updateBtn();
	  });
	}
 function subscribeUser() {
	  const applicationServerKey = window.vapidPublicKey;
	  swRegistration.pushManager.subscribe({
	    userVisibleOnly: true,
	    applicationServerKey: applicationServerKey
	  })
	  .then(function(subscription) {
	    console.log('User is subscribed:', subscription);

	    updateSubscriptionOnServer(subscription);

	    isSubscribed = true;

	   // updateBtn();
	  })
	  .catch(function(err) {
	    console.log('Failed to subscribe the user: ', err);
	   // updateBtn();
	  });
	}